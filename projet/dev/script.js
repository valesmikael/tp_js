var $ai=document.getElementById('AI');
var $joueur=document.getElementById('joueur');
var $littleboat=document.getElementsByClassName('littleboat');
var $middleboat=document.getElementsByClassName('middleboat');
var $bigboat=document.getElementsByClassName('bigboat');
var $ailittleboat=document.getElementsByClassName('ailittleboat');
var $aimiddleboat=document.getElementsByClassName('aimiddleboat');
var $aibigboat=document.getElementsByClassName('aibigboat');
var $explosion=document.getElementById('explosion');
var $explosionadverse=document.getElementById('explosionadverse');

var clicklittleboat=0;
var clickmiddleboat=0;
var clickbigboat=0;

var aiclicklittleboat=0;
var aiclickmiddleboat=0;
var aiclickbigboat=0;


//****************************************Ce qui se passe quand l'IA joue***********************************
function iaPlay(){
//*****récuperer tous les td du tableau $joueur*****
var tableau = $joueur.querySelectorAll("td");
    //console.log(tableau);
//*****récupere le nombre de td du tableau $joueur*****
var tableauTaille = tableau.length;
    //console.log(tableauTaille);
//*****pêcher un nombre aléatoire*****
function getRandomInt(max) {
return Math.floor(Math.random() * Math.floor(max));
}
var chiffreAleat = getRandomInt(tableauTaille);
    //console.log(chiffreAleat);
//*****associé le chiffre aléatoire au td pour récuperer son contenu*****
var result = tableau[chiffreAleat];

    if (result.getAttribute('data-touche','true')){
        iaPlay();
        console.log('déja touché');
    }else{
        
    // On compte le nombre de click fait sur chaque class en laissant un commentaire si le bateau est coulé.
    if (result.className == 'littleboat'){
        aiclicklittleboat += 1;
        $explosionadverse.style.display='inline-block';
        setTimeout( boomboom ,1200);
        if(aiclicklittleboat === 2){alert( "L'ia vous a coulé votre sous-marin")}};
        
    
    if (result.className == 'middleboat'){
        aiclickmiddleboat += 1;
        $explosionadverse.style.display='inline-block';
        setTimeout( boomboom ,1200);
        if(aiclickmiddleboat === 3){alert( "L'ia vous a coulé votre destroyer")}};
    
    if (result.className == 'bigboat'){
        aiclickbigboat += 1;
        $explosionadverse.style.display='inline-block';
        setTimeout( boomboom ,1200);
        if(aiclickbigboat === 5){alert( "L'ia vous a coulé votre port-avion")}};
    
    // on colorie les cases selon la réponse reçu au click  
    if (result.className != 'green' && result.className.length > 0  ){
        result.style.backgroundColor='green'; 
        result.className="green"; 
        } else if( result.className.length == 0 ) { 
        result.style.backgroundColor='blue';
    };

    if (result.className==0){
        play;
    }else{
    window.setTimeout("iaPlay()",1150);};
       
    //message quand tous les bateaux sont coulés.
    if (aiclicklittleboat==2 && aiclickmiddleboat==3 && aiclickbigboat==5 ){
        alert('Dommage, votre flotte a été anéantie');
    };

    //a revoir
    //ajout d'une class quand on touche
    if (result){
        result.setAttribute('data-touche','true');
        console.log(result);       
    }
    //n'enregistre pas si on touche sur la même case
    
}};

function boom(){
    $explosion.style.display='none';
}

function boomboom(){
    $explosionadverse.style.display='none';
}
    

//****************************************Ce qui se passe quand le joueur joue**********************************

//on écoute pour savoir si il y a un click sur le tableau adversaire
var play=$ai.addEventListener('click',function(event){
    let cible = event.target;


// On compte le nombre de click fait sur chaque class en laissant un commentaire si le bateau est coulé.
if (cible.className == 'ailittleboat'){
    clicklittleboat += 1;
    $explosion.style.display='inline-block';
    setTimeout( boom ,1200);
    if(clicklittleboat === 2){alert( "Vous avez coulé le sous-marin adverse")}};
    

if (cible.className == 'aimiddleboat'){
    clickmiddleboat += 1;
    $explosion.style.display='inline-block';
    setTimeout( boom ,1200);
    if(clickmiddleboat === 3){alert( "Vous avez coulé le destroyer adverse")}};

if (cible.className == 'aibigboat'){
    clickbigboat += 1;
    $explosion.style.display='inline-block';
    setTimeout( boom ,1200);
    if(clickbigboat === 5){alert( "Vous avez coulé le port-avion adverse")}};

// on colorie les cases selon la réponse reçu au click  et on interdit de cliquer au même endroit si déja cliqué une fois
if (cible.className != 'orange' && cible.className.length > 0  ){
    cible.style.backgroundColor='orange'; 
    cible.className="orange";  
} else if( cible.className.length == 0 ) { 
    cible.style.backgroundColor='blue';
};

if (cible.className==0){
    window.setTimeout("iaPlay()",1150);
};

//message quand tous les bateaux sont coulés.
if (clicklittleboat==2 && clickmiddleboat==3 && clickbigboat==5 ){
    alert('Bravo, vous avez coulé la flotte adverse');
};
//****a revoir****
//ajout d'une class quand on touche
if (cible){
    cible.setAttribute('data-touche','true');
    console.log(cible);
     
}});

